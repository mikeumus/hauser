#!/bin/bash
set -e

echo "Generating config..."
# echo $GOOGLE_KEY_JSON > /server-conf/google_key.json
# export GOOGLE_APPLICATION_CREDENTIALS=/server-conf/google_key.json

envsubst < /server-conf/config.toml.tmpl > /server-conf/config.toml
# cat /server-conf/config.toml
# cat /server-conf/google_key.json

echo "Pinging Postgres in 2 seconds..."
sleep 2
while ! curl http://postgres:master-password@postgres:5432/dev 2>&1 | grep '52'
do
  echo "No response from Postgres. Sleep 1 sec, trying again..."
  sleep 1
done
echo "Response received from Postgres."

echo "Starting hauser..."
hauser -c /server-conf/config.toml
