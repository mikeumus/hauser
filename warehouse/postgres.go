package warehouse

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/lib/pq"
	"github.com/nishanths/fullstory"

	"github.com/fullstorydev/hauser/config"
)

type Postgres struct {
	conn         *sql.DB
	conf         *config.Config
	exportSchema Schema
	syncSchema   Schema
}

var (
	PostgresTypeMap = FieldTypeMapper{
		"int64":     "BIGINT",
		"string":    "VARCHAR",
		"time.Time": "VARCHAR",
	}
	beginningOfPqlTime = time.Date(2015, 01, 01, 0, 0, 0, 0, time.UTC)
)

func NewPostgres(c *config.Config) *Postgres {
	return &Postgres{
		conf:         c,
		exportSchema: ExportTableSchema(PostgresTypeMap),
		syncSchema:   SyncTableSchema(PostgresTypeMap),
	}
}

func (pql *Postgres) qualifiedExportTableName() string {
	if pql.conf.Postgres.DatabaseSchema == "search_path" {
		return pql.conf.Postgres.ExportTable
	}
	return fmt.Sprintf("%s.%s", pql.conf.Postgres.DatabaseSchema, pql.conf.Postgres.ExportTable)
}

func (pql *Postgres) qualifiedSyncTableName() string {
	if pql.conf.Postgres.DatabaseSchema == "search_path" {
		return pql.conf.Postgres.SyncTable
	}
	return fmt.Sprintf("%s.%s", pql.conf.Postgres.DatabaseSchema, pql.conf.Postgres.SyncTable)
}

func (pql *Postgres) getSchemaParameter() string {
	// the built-in current_schema() function will walk the Postgres search_path to get a schema name
	// more info: https://www.postgresql.org/docs/9.4/functions-info.html
	if pql.conf.Postgres.DatabaseSchema == "search_path" {
		return "current_schema()"
	}

	return fmt.Sprintf("'%s'", pql.conf.Postgres.DatabaseSchema)
}

func (pql *Postgres) validateSchemaConfig() error {
	if pql.conf.Postgres.DatabaseSchema == "" {
		return errors.New("DatabaseSchema definition missing from Postgres configuration. More information: https://github.com/fullstorydev/hauser/blob/master/Postgres.md#database-schema-configuration")
	}
	return nil
}

// GetExportTableColumns returns all the columns of the export table.
// It opens a connection and calls getTableColumns
func (pql *Postgres) GetExportTableColumns() []string {
	var err error
	pql.conn, err = pql.MakePostgresConnection()
	if err != nil {
		log.Fatal(err)
	}
	defer pql.conn.Close()

	return pql.getTableColumns(pql.conf.Postgres.ExportTable)
}

func (pql *Postgres) ValueToString(val interface{}, isTime bool) string {
	s := fmt.Sprintf("%v", val)
	if isTime {
		t, _ := time.Parse(time.RFC3339Nano, s)
		return t.String()
	}

	s = strings.Replace(s, "\n", " ", -1)
	s = strings.Replace(s, "\r", " ", -1)
	s = strings.Replace(s, "\x00", "", -1)

	if len(s) >= pql.conf.Postgres.VarCharMax {
		s = s[:pql.conf.Postgres.VarCharMax-1]
	}
	return s
}

func (pql *Postgres) MakePostgresConnection() (*sql.DB, error) {
	if err := pql.validateSchemaConfig(); err != nil {
		log.Fatal(err)
	}
	url := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		pql.conf.Postgres.User,
		pql.conf.Postgres.Password,
		pql.conf.Postgres.Host,
		pql.conf.Postgres.Port,
		pql.conf.Postgres.DB)

	var err error
	var db *sql.DB
	if db, err = sql.Open("postgres", url); err != nil {
		return nil, fmt.Errorf("Postgres connect error : (%v)", err)
	}

	if err = db.Ping(); err != nil {
		return nil, fmt.Errorf("Postgres ping error : (%v)", err)
	}
	return db, nil
}

func (pql *Postgres) UploadFile(filename string) (string, error) {
	return filename, nil
}

func (pql *Postgres) DeleteFile(filename string) {
	var err = os.Remove(filename)
	if err != nil {
		log.Printf("Error deleting file: %s", filename)
	}

	log.Printf("Done deleting file: %s", filename)
}

func (pql *Postgres) LoadToWarehouse(filename string, _ ...fullstory.ExportMeta) error {
	var err error
	pql.conn, err = pql.MakePostgresConnection()
	if err != nil {
		return err
	}
	defer pql.conn.Close()

	if err = pql.CopyInData(filename); err != nil {
		return err
	}

	return nil
}

// EnsureCompatibleExportTable makes sure the export table has all the hauser schema columns
func (pql *Postgres) EnsureCompatibleExportTable() error {
	var err error
	pql.conn, err = pql.MakePostgresConnection()
	if err != nil {
		return err
	}
	defer pql.conn.Close()

	if !pql.DoesTableExist(pql.conf.Postgres.ExportTable) {
		// if the export table does not exist we create one with all the columns we expect!
		log.Printf("Export table %s does not exist! Creating one!", pql.qualifiedExportTableName())
		if err = pql.CreateExportTable(); err != nil {
			return err
		}
		return nil
	}

	// make sure all the columns in the csv export exist in the Export table
	exportTableColumns := pql.getTableColumns(pql.conf.Postgres.ExportTable)
	missingFields := pql.getMissingFields(pql.exportSchema, exportTableColumns)

	// If some fields are missing from the fsexport table, either we added new fields
	// or existing expected columns were deleted by the user we add the relevant columns.
	// Alter the table and add the missing columns.
	if len(missingFields) > 0 {
		log.Printf("Found %d missing fields. Adding columns for these fields.", len(missingFields))
		for _, f := range missingFields {
			// Postgres only allows addition of one column at a time, hence the the alter statements in a loop yuck
			alterStmt := fmt.Sprintf("ALTER TABLE %s ADD COLUMN %s %s;", pql.qualifiedExportTableName(), f.Name, f.DBType)
			if _, err = pql.conn.Exec(alterStmt); err != nil {
				return err
			}
		}
	}
	return nil
}

// CopyInData copies data from the given filename to the export table
func (pql *Postgres) CopyInData(filename string) error {
	copyStatement := fmt.Sprintf("COPY %s FROM '%s' DELIMITER ',' CSV NULL '';",
		pql.qualifiedExportTableName(), filename)
	_, err := pql.conn.Exec(copyStatement)
	return err
}

// CreateExportTable creates an export table with the hauser export table schema
func (pql *Postgres) CreateExportTable() error {
	log.Printf("Creating export table %s", pql.qualifiedExportTableName())

	stmt := fmt.Sprintf("create table %s(%s);", pql.qualifiedExportTableName(), pql.exportSchema.String())
	_, err := pql.conn.Exec(stmt)
	return err
}

// CreateSyncTable creates a sync table with the hauser sync table schema
func (pql *Postgres) CreateSyncTable() error {
	log.Printf("Creating sync table %s", pql.qualifiedSyncTableName())

	stmt := fmt.Sprintf("create table %s(%s);", pql.qualifiedSyncTableName(), pql.syncSchema.String())
	_, err := pql.conn.Exec(stmt)
	return err
}

func (pql *Postgres) SaveSyncPoints(bundles ...fullstory.ExportMeta) error {
	var err error
	pql.conn, err = pql.MakePostgresConnection()
	if err != nil {
		log.Printf("Couldn't connect to DB: %s", err)
		return err
	}
	defer pql.conn.Close()

	for _, e := range bundles {
		insert := fmt.Sprintf("insert into %s values (%d, '%s', '%s')",
			pql.qualifiedSyncTableName(), e.ID, time.Now().Format(time.RFC3339), e.Stop.Format(time.RFC3339))
		if _, err := pql.conn.Exec(insert); err != nil {
			return err
		}
	}

	return nil
}

func (pql *Postgres) DeleteExportRecordsAfter(end time.Time) error {
	stmt := fmt.Sprintf("DELETE FROM %s where EventStart > '%s';",
		pql.qualifiedExportTableName(), end.Format(time.RFC3339))
	_, err := pql.conn.Exec(stmt)
	if err != nil {
		log.Printf("failed to delete from %s: %s", pql.qualifiedExportTableName(), err)
		return err
	}

	return nil
}

func (pql *Postgres) LastSyncPoint() (time.Time, error) {
	t := beginningOfPqlTime
	var err error
	pql.conn, err = pql.MakePostgresConnection()
	if err != nil {
		log.Printf("Couldn't connect to DB: %s", err)
		return t, err
	}
	defer pql.conn.Close()

	if pql.DoesTableExist(pql.conf.Postgres.SyncTable) {
		var syncTime pq.NullTime
		q := fmt.Sprintf("SELECT max(BundleEndTime) FROM %s;", pql.qualifiedSyncTableName())
		if err := pql.conn.QueryRow(q).Scan(&syncTime); err != nil {
			log.Printf("Couldn't get max(BundleEndTime): %s", err)
			return t, err
		}
		if syncTime.Valid {
			t = syncTime.Time
		}

		if err := pql.RemoveOrphanedRecords(syncTime); err != nil {
			return t, err
		}

	} else {
		if err := pql.CreateSyncTable(); err != nil {
			log.Printf("Couldn't create sync table: %s", err)
			return t, err
		}
	}
	return t, nil
}

func (pql *Postgres) RemoveOrphanedRecords(lastSync pq.NullTime) error {
	if !pql.DoesTableExist(pql.conf.Postgres.ExportTable) {
		if err := pql.CreateExportTable(); err != nil {
			log.Printf("Couldn't create export table: %s", err)
			return err
		}
	}

	// Find the time of the latest export record...if it's after
	// the time in the sync table, then there must have been a failure
	// after some records have been loaded, but before the sync record
	// was written. Use this as the latest sync time, and don't load
	// any records before this point to prevent duplication
	var exportTime pq.NullTime
	q := fmt.Sprintf("SELECT max(EventStart) FROM %s;", pql.qualifiedExportTableName())
	if err := pql.conn.QueryRow(q).Scan(&exportTime); err != nil {
		log.Printf("Couldn't get max(EventStart): %s", err)
		return err
	}
	if exportTime.Valid && exportTime.Time.After(lastSync.Time) {
		log.Printf("Export record timestamp after sync time (%s vs %s); cleaning",
			exportTime.Time, lastSync.Time)
		pql.DeleteExportRecordsAfter(lastSync.Time)
	}

	return nil
}

// DoesTableExist checks if a table with a given name exists
func (pql *Postgres) DoesTableExist(name string) bool {
	log.Printf("Checking if table %s exists", name)

	var exists int
	query := fmt.Sprintf("SELECT count(*) FROM information_schema.tables WHERE table_schema = %s AND table_name = $1;", pql.getSchemaParameter())
	err := pql.conn.QueryRow(query, name).Scan(&exists)
	if err != nil {
		// something is horribly wrong...just give up
		log.Fatal(err)
	}
	return (exists != 0)
}

func (pql *Postgres) getTableColumns(name string) []string {
	log.Printf("Fetching columns for table %s", name)
	ctx := context.Background()
	query := fmt.Sprintf("SELECT column_name FROM information_schema.columns WHERE table_schema = %s AND table_name = $1 order by ordinal_position;", pql.getSchemaParameter())
	rows, err := pql.conn.QueryContext(ctx, query, name)
	if err != nil {
		log.Fatal(err)
	}
	var columns []string

	defer rows.Close()
	for rows.Next() {
		var column string
		if err = rows.Scan(&column); err != nil {
			log.Fatal(err)
		}
		columns = append(columns, column)
	}

	// get any error encountered during iteration
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
	return columns
}

func (pql *Postgres) getMissingFields(schema Schema, tableColumns []string) []WarehouseField {
	existingColumns := make(map[string]struct{})
	for _, column := range tableColumns {
		// Postgres columns are case insensitive
		existingColumns[strings.ToLower(column)] = struct{}{}
	}

	var missingFields []WarehouseField
	for _, f := range schema {
		if _, ok := existingColumns[strings.ToLower(f.Name)]; !ok {
			missingFields = append(missingFields, f)
		}
	}

	return missingFields
}

func (pql *Postgres) GetUploadFailedMsg(filename string, err error) string {
	return fmt.Sprintf("Failed to copy file %s to local dir %s: %s", filename, pql.conf.Local.SaveDir, err)
}

func (pql *Postgres) IsUploadOnly() bool {
	// We need to return false, otherwise sync point will never be saved
	return false
}
